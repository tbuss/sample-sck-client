package de.adesso.samplesckclient

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
class SampleSckClientApplication

fun main(args: Array<String>) {
    runApplication<SampleSckClientApplication>(*args)
}
