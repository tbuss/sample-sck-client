package de.adesso.samplesckclient

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity

@RestController
class SampleSckClientController {

    @Autowired
    lateinit var discoveryClient: DiscoveryClient

    @Value("\${SERVICE_NAME}")
    lateinit var serviceName: String

    @Value("\${RESOURCE_PATH}")
    lateinit var resourcePath: String

    @GetMapping("/")
    fun doGet(): String = getResultFromService()

    fun getResultFromService(): String {
        val response: ResponseEntity<String> = RestTemplate().getForEntity("http://$serviceName:8080/$resourcePath", String)
        return if (response.hasBody()) response.body!!
        else "default"
    }
}
